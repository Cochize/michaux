<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Le script de création wp-config.php utilise ce fichier lors de l'installation.
 * Vous n'avez pas à utiliser l'interface web, vous pouvez directement
 * renommer ce fichier en "wp-config.php" et remplir les variables à la main.
 * 
 * Ce fichier contient les configurations suivantes :
 * 
 * * réglages MySQL ;
 * * clefs secrètes ;
 * * préfixe de tables de la base de données ;
 * * ABSPATH.
 * 
 * @link https://codex.wordpress.org/Editing_wp-config.php 
 * 
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'michaux');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JW(YHjTKY{efAw}!?+!V8PXH/D!8y@u12.:soM:0QL}b|/6AeV{)(p^gcf,0238C');
define('SECURE_AUTH_KEY',  ';UOhzpP@&]J.wK<[B8FB)q qLAbgs_o%1E=w&{=q%Ew/E;!gCo!}9bGRL06^hm#}');
define('LOGGED_IN_KEY',    '5yrvbW_h:P,1xFd-=^R!g_cC-zWt(R9r4H;iA.p[6?[Ykq)b]JVvKeY_+tdNG-ak');
define('NONCE_KEY',        'sV5oheo?2HtZ8T-vZci`HF|*4Z=G`@FVnnuctt<Ygm:#fY4by:|BnI})?/ZciOI_');
define('AUTH_SALT',        'Jn}x!Dabyk`WB<q5!z0*%_Ax$a0rZ-lDDej&>~h5ra;*8KQVS.Jss6:BBIpuAJNp');
define('SECURE_AUTH_SALT', '~Kub*>dIUQZ36`i#U/7Len!B-s4i^4;nv9WmT(RMNu-k+!zZnJrVNtq|+=F}fh|B');
define('LOGGED_IN_SALT',   'Ck$.@1CcI7>8DC`4u%;;k /ho8sd#!/@iei;Ql3|7w16et/nXjkBq5`_T[1#SxmW');
define('NONCE_SALT',       'tUeQ>%d^>0&B{JL,|;T*,^+Sx,sIt kn$|MAm~Je<qHk7Tgl#sLyMIHeF}Kp.ya1');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'dmi_';

/** 
 * Pour les développeurs : le mode déboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 * 
 * Pour obtenir plus d'information sur les constantes 
 * qui peuvent être utilisée pour le déboguage, consultez le Codex.
 * 
 * @link https://codex.wordpress.org/Debugging_in_WordPress 
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');