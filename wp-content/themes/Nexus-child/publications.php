<?php
/*
Template Name: Publications
*/
?>
<?php get_header(); ?>

<?php
$featured_image = false;

if ( '' != get_the_post_thumbnail() ) :
	$featured_image = true;
?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> <!-- .container -->
</div> <!-- .post-thumbnail -->
<?php endif; ?>

<div id="main-publication">
	<div class="container">
		<h1 class="main-title">Voix Solidaires</h1>

							<?php for ($i=1; $i <= 1; $i++) { ?>
								<?php query_posts('category_name=voix-solidaires&showposts=1'); while (have_posts()) : the_post(); ?>
									<?php $exemple_metas = get_post_custom(); ?>
									
<iframe src="//v.calameo.com/?bkcode=<?php echo $exemple_metas['calameo'][0];?>&showsharemenu=false&clickto=view&clicktarget=_self" width="100%" height="600" frameborder="0" scrolling="no" allowtransparency allowfullscreen style="margin:0 auto;"></iframe>
	
								<?php endwhile; wp_reset_query(); ?>
							<?php } ?>


						
						<div id="et-post-share" class="clearfix">
							<span><?php esc_html_e( 'Partager sur', 'Nexus' ); ?></span>
							<ul id="et-share-icons">
							<?php
								$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' );
								$title_attribute = the_title_attribute( 'echo=0' );
								$post_permalink  = get_permalink();

								printf( '<li class="google-share"><a href="https://plus.google.com/share?url=%s" target="_blank" class="et-share-button et-share-google">%s</a></li>',
									esc_url( $post_permalink ),
									esc_html__( 'Google', 'Nexus' )
								);

								printf( '<li class="facebook-share"><a href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=%1$s&amp;p[images][0]=%2$s&amp;p[title]=%3$s" target="_blank" class="et-share-button et-share-facebook">%4$s</a></li>',
									esc_url( $post_permalink ),
									esc_attr( $thumbnail[0] ),
									$title_attribute,
									esc_html__( 'Facebook', 'Nexus' )
								);

								printf( '<li class="twitter-share"><a href="https://twitter.com/intent/tweet?url=%1$s&amp;text=%2$s" target="_blank" class="et-share-button et-share-twitter">%3$s</a></li>',
									esc_url( $post_permalink ),
									$title_attribute,
									esc_html__( 'Twitter', 'Nexus' )
								);
							?>
							</ul>
						</div>
				

		</div>
</div>

<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">


				<div id="left-area">
					<div id="half1">
						<h2>&Eacute;ditions précédentes</h2>

							<?php for ($i=1; $i <= 1; $i++) { ?>
								<?php query_posts('category_name=voix-solidaires&offset=1'); while (have_posts()) : the_post(); ?>
									<?php 
										global $more; $more = 0;

											$thumb = '';
											$width = 150;
											$height = 150;
				
											$classtext = 'item-image';
											$titletext = get_the_title();
											$thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext,false,'etservice');
											$thumb = $thumbnail["thumb"];
											$et_service_link = get_post_meta($post->ID,'etlink',true) ? get_post_meta($post->ID,'etlink',true) : get_permalink();
										?>
										<div class="livret">
			


											<a href="<?php echo $et_service_link; ?>">
												<?php if (class_exists('MultiPostThumbnails')) :
												    MultiPostThumbnails::the_post_thumbnail(
												        get_post_type(),
												        'secondary-image'
												    );
												endif; ?>
											</a>



											<h3><a href="<?php echo $et_service_link; ?>"><?php the_title(); ?></a></h3>
										</div> 

								<?php endwhile; wp_reset_query(); ?>
							<?php } ?>
					</div>
					<div id="half2">
						<?php while ( have_posts() ) : the_post(); ?>

							<article class="entry-content clearfix">
							<?php
								the_content();

								wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
							?>
							</article> <!-- .entry -->

							<?php
								if ( comments_open() && 'on' == et_get_option( 'nexus_show_pagescomments', 'false' ) )
									comments_template( '', true );
							?>

						<?php endwhile; ?>
					</div>

				</div> 	<!-- end #left-area -->
			</div> <!-- #content -->
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

<?php get_footer(); ?>