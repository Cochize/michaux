<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<style>
	#featured { display:none; }
	#main-header .container{ width: 960px; left:0%; margin:0; padding:0; }
	.footer-banner{ width: 960px; left:0%; margin:0; padding:0; }
	#main-content{ width: 960px; left:0%; margin:0; padding:0; }
	#main-content #content{width: 600px;}
	#main-footer{ width: 960px; left:0%; margin:0; padding:0; }
</style>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title><?php elegant_titles(); ?></title>
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="http://localhost/MICHAUX/xmlrpc.php" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>

	<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="favicons/android-chrome-manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">	

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-60171842-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<!--Font Awesome-->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
	
	<!-- Hover.css -->
	<link href="wp-content/themes/Nexus-child/lib/hover/hover.css" rel="stylesheet" media="all">
	<link href="wp-content/themes/Nexus-child/my-style.css" rel="stylesheet" media="all">
</head>
<body <?php body_class(); ?>>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&appId=369599213210211&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



	<header id="main-header">
		<div class="container">

			<?php
				if ( is_front_page() ) {
			?>

			<h1 id="top-info" class="clearfix">
				<a alt="Michaux Déménagements" id="logo" href="http://localhost/MICHAUX/">Michaux Déménagements</a>
			</h1>

			<?php
			} else {
			?>
			<h1 id="top-info" class="clearfix">
				<a alt="Michaux Déménagements" id="logo" href="http://localhost/MICHAUX/">Michaux Déménagements</a>
			</h1>
			<?php
			}
			?>
			<div id="post-header">
				<div class="container">
				<div id="social">
					<a href="http://www.facebook.com/" target="_blank"><img src="http:\\localhost\Michaux\wp-content\themes\Nexus\images\facebook.jpg"></a>
				</div>
				<?php
					$menu_class = 'bottom-nav';
					$footerNav = '';

					$footerNav = wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menu_class, 'echo' => false, 'depth' => '1' ) );

					if ( '' === $footerNav )
						show_page_menu( $menu_class );
					else
						echo( $footerNav );

				?>
				</div>
			</div>
			<div id="top-navigation" class="clearfix">
				<?php do_action( 'et_header_top' ); ?>

				<nav>
				<?php
					$menuClass = 'nav';
					if ( 'on' == et_get_option( 'nexus_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
					$primaryNav = '';

					$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

					if ( '' == $primaryNav ) :
				?>
					<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
						<?php if ( 'on' == et_get_option( 'nexus_home_link' ) ) { ?>
							<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home','Nexus' ); ?></a></li>
						<?php }; ?>

						<?php show_page_menu( $menuClass, false, false ); ?>
						<?php show_categories_menu( $menuClass, false ); ?>
					</ul>
				<?php
					else :
						echo( $primaryNav );
					endif;
				?>
				</nav>
			</div> <!-- #top-navigation -->
			
		</div> <!-- .container -->
	</header> <!-- #main-header -->
	


			<?php
				if ( is_front_page() ) {
			?>
	<div id="slide">
				<?php 
					echo do_shortcode('[masterslider id="3"]');
				?>
	</div>
	<div class="container">
		<div id="main-grille">
			<a class="hvr-underline-reveal boxlink1" href="index.php/mon-demenagements/">
				<h3>DÉMÉNAGEMENT</h3>
				<h2>En savoir plus</h2>
			</a>
			<a class="hvr-underline-reveal boxlink2" href="index.php/service-lift/">
				<h3>SERVICE LIFT</h3>
				<h2>En savoir plus</h2>
			</a>
			<a class="hvr-underline-reveal boxlink3" href="index.php/garde-meubles/">
				<h3>GARDE-MEUBLES</h3>
				<h2>En savoir plus</h2>
			</a>
			<a class="hvr-underline-reveal boxlink4" href="index.php/fournitures/">
				<h3>FOURNITURES</h3>
				<h2>En savoir plus</h2>
			</a>
		</div>
	</div>

	<div id="main-accueil">
		<div class="container">
			<div class="half" id="atout">
				<?php get_sidebar( 'additional' ); ?>

			</div>
			<div class="half" id="nous">
				<?php get_sidebar( 'additional-2' ); ?>
			</div>
		</div>
	</div>


	<?php
		} else {
	?>

	<?php
		}
	?>
