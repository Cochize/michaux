<?php
/*
Template Name: Projets
*/
?>
<?php get_header(); ?>

<?php
$featured_image = true;

?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> 
</div> 


<div class="page-wrap container">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">

				<?php while ( have_posts() ) : the_post(); ?>

					<article class="entry-content clearfix">
					
					<h1 class="main-title"><?php the_title(); ?></h1>
					aaa
					<?php
						the_content();

						wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
					?>
					</article> <!-- .entry -->

					
					<?php if (et_get_option('nexus_integration_single_bottom') <> '' && et_get_option('nexus_integrate_singlebottom_enable') == 'on') echo(et_get_option('nexus_integration_single_bottom')); ?>

					<div id="et-box-author">
						<div id="et-bio-author">
							<div class="author-avatar">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 60 ); ?>
							</div> <!-- end #author-avatar -->

							<p id="author-info">
								<strong><?php esc_html_e( 'Author', 'Nexus' ); ?>:</strong> <?php the_author_link(); ?>
							</p> <!-- end #author-info -->

							<p><?php the_author_meta( 'description' ); ?></p>
						</div>

						<div id="et-post-share" class="clearfix">
							<span><?php esc_html_e( 'Partager sur', 'Nexus' ); ?></span>
							<ul id="et-share-icons">
							<?php
								$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' );
								$title_attribute = the_title_attribute( 'echo=0' );
								$post_permalink  = get_permalink();

								printf( '<li class="google-share"><a href="https://plus.google.com/share?url=%s" target="_blank" class="et-share-button et-share-google">%s</a></li>',
									esc_url( $post_permalink ),
									esc_html__( 'Google', 'Nexus' )
								);

								printf( '<li class="facebook-share"><a href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=%1$s&amp;p[images][0]=%2$s&amp;p[title]=%3$s" target="_blank" class="et-share-button et-share-facebook">%4$s</a></li>',
									esc_url( $post_permalink ),
									esc_attr( $thumbnail[0] ),
									$title_attribute,
									esc_html__( 'Facebook', 'Nexus' )
								);

								printf( '<li class="twitter-share"><a href="https://twitter.com/intent/tweet?url=%1$s&amp;text=%2$s" target="_blank" class="et-share-button et-share-twitter">%3$s</a></li>',
									esc_url( $post_permalink ),
									$title_attribute,
									esc_html__( 'Twitter', 'Nexus' )
								);
							?>
							</ul>
						</div>
					</div>
					
					
					<?php
						if ( comments_open() && 'on' == et_get_option( 'nexus_show_pagescomments', 'false' ) )
							comments_template( '', true );
					?>

				<?php endwhile; ?>

				</div> 	<!-- end #left-area -->
			</div> <!-- #content -->

			<?php get_sidebar(); ?>
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

	<?php get_footer(); ?>