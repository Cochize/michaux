<?php if ( is_active_sidebar( 'sidebar-additional-2' ) && 'on' === et_get_option( 'nexus_3rd_column', 'on' ) ) : ?>
	<div id="additional-sidebar-2">
		<?php dynamic_sidebar( 'sidebar-additional-2' ); ?>
	</div> <!-- #additional-sidebar -->
<?php endif; ?>