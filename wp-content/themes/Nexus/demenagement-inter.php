<?php
/*
Template Name: Demenagement internationaux
*/
?>
<?php get_header(); ?>

<?php
$featured_image = false;

if ( '' != get_the_post_thumbnail() ) :
	$featured_image = true;
?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> <!-- .container -->
</div> <!-- .post-thumbnail -->
<?php endif; ?>
<div class="container2">
	<div class="sub-header">
		<div class="sub-left5">
			<h1>ENVIE DE NOUVEAUX HORIZONS ? DÉMÉNAGER À L’ÉTRANGER … <br />PAS SI SIMPLE ?</h1>
			<p>Vous n’êtes pas le premier, la première, à envisager un changement de pays lié à un changement de vie.
Les raisons peuvent être multiples, autant personnelles (envie de profiter de sa pension au soleil,…) que professionnelles.
Mais les questions qui se posent devant les aspects pratiques sont souvent les mêmes.</p>
		</div>
		<div class="sub-right2">
			<div id="align-sub-right2">
			<ul>
			<li><i class="fa2 fa-thumbs-o-up"></i> A quel moment dois-je contacter les déménageurs ?</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> Comment puis-je préparer le jour de mon déménagement ?</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> Combien de temps à l'avance dois-je réserver mon déménagement ?</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> Combien coûte un déménagement à l'étranger ?</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> Mes affaires sont-elles assurées par les déménageurs ?</li>
			</ul>
			<div id="btn-align">
			<a href="index.php/contact/" class="btn-my-animation-border hvr-wobble-horizontal">
			Réservez votre déménagement
			<i class="fa fa-arrow-right"></i>
			</a>
			</div>
			</div>
		</div>
	</div>
	<div id="main-grille">
		<a class="hvr-underline-reveal boxlink1" href="index.php/mon-demenagements/">
			<h3>DÉMÉNAGEMENT</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink2" href="index.php/service-lift/">
			<h3>SERVICE LIFT</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink3" href="index.php/garde-meubles/">
			<h3>GARDE-MEUBLES</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink4" href="index.php/fournitures/">
			<h3>FOURNITURES</h3>
			<h2>En savoir plus</h2>
		</a>
	</div>
</div>
<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">

				<?php while ( have_posts() ) : the_post(); ?>

					<article class="entry-content clearfix">
					<?php if ( ! $featured_image ) : ?>
					<?php endif; ?>
					<?php
						the_content();

						wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
					?>
					</article> <!-- .entry -->

					<?php
						if ( comments_open() && 'on' == et_get_option( 'nexus_show_pagescomments', 'false' ) )
							comments_template( '', true );
					?>

				<?php endwhile; ?>

				</div> 	<!-- end #left-area -->

			</div> <!-- #content -->
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

<?php get_footer(); ?>