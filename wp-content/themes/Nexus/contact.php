<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<?php
$featured_image = false;

if ( '' != get_the_post_thumbnail() ) :
	$featured_image = true;
?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> <!-- .container -->
</div> <!-- .post-thumbnail -->
<?php endif; ?>
<div class="container">
	<div class="sub-header">
		<div class="sub-left4">
			<h1>Besoin d'informations complémentaires ?<br />
			Vous désirez plannifier votre déménagement ? </h1>
		</div>
		<div class="sub-right">
		<?php 
					echo do_shortcode('[masterslider id="4"]');
				?>
		</div>
	</div>
	<div id="form-content">

		<div id="form-left">
			<img src="http:\\localhost\Michaux\wp-content\themes\Nexus\images\logo_michaux_demenagement_gris.jpg">
			<p>
			Gsm		+32 (0) 473 72 05 91<br />
 			+32 (0)  475 54 08 98<br />
			Fax 		+32 (0) 82 61 57 92<br />
E-mail	<span><a href="mailto:info@demenagements-michaux.be">info@demenagements-michaux.be</a></span>
</p>
<p>
LUNDI <span> 8h     >   19h</span><br />
MARDI <span>8h     >   19h</span><br />
MERCREDI <span>8h     >   19h</span><br />
JEUDI <span>8h     >   19h</span><br />
VENDREDI <span>8h     >   19h</span><br />
SAMEDI <span>8h     >   16h</span><br />
</p>
<p>
<b>ADRESSE DU BUREAU</b><br />

Chemin des Pommiers, 34<br />
B-5500 Dinant<br /><br />

<b>ADRESSE DU GARDE-MEUBLES</b><br />

Chemin du Bois aux Mouches, 42<br />
B-5590 Achêne - Ciney
			</p>
		</div>
		<div id="form-right">
				<?php 
					echo do_shortcode('[contact-form-7 id="613" title="Sans titre"]');
				?>
		</div>

	</div>
<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">

				<?php while ( have_posts() ) : the_post(); ?>

					<article class="entry-content clearfix">
					<?php if ( ! $featured_image ) : ?>
					<?php endif; ?>
					<?php
						the_content();

						wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
					?>
					</article> <!-- .entry -->

					<?php
						if ( comments_open() && 'on' == et_get_option( 'nexus_show_pagescomments', 'false' ) )
							comments_template( '', true );
					?>

				<?php endwhile; ?>

				</div> 	<!-- end #left-area -->

			</div> <!-- #content -->
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

<?php get_footer(); ?>