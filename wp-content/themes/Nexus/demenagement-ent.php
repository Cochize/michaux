<?php
/*
Template Name: Demenagement entreprise
*/
?>
<?php get_header(); ?>

<?php
$featured_image = false;

if ( '' != get_the_post_thumbnail() ) :
	$featured_image = true;
?>
<div class="post-thumbnail">
	<div class="container">
		<h1 class="post-heading"><?php the_title(); ?></h1>
	</div> <!-- .container -->
</div> <!-- .post-thumbnail -->
<?php endif; ?>
<div class="container2">
	<div class="sub-header">
		<div class="sub-left6">
			<h1>BUREAUX, ARCHIVES, MATÉRIEL… QUELLES SOLUTIONS ?</h1>
			<p>Dans le cadre d’une activité professionnelle, un déménagement représente avant tout de nombreuses contraintes.</p>
			<a href="index.php/contact/" class="btn-my-animation-border hvr-wobble-horizontal">
			Contactez nous 
			<i class="fa fa-arrow-right"></i>
			</a>
		</div>
		<div class="sub-right2">
		<div id="align-sub-right">
		<p>Quelle que soit la nature des choses à déplacer, la distance à parcourir ou les difficultés liées aux locaux, vous pensez que :</p>
			<ul>
			<li><i class="fa2 fa-thumbs-o-up"></i> Cela représente un coût sans valeur ajoutée.</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> C'est synonyme d’un arrêt de votre activité durant ce transfert.</li>
			<li><i class="fa2 fa-thumbs-o-up"></i> Cela représente des risques pour vos employés, vos équipements et vos locaux.</li>
			</ul>
			<p>Les Déménagements Michaux & fils peuvent apporter des solutions qui répondront à ces craintes.</p>
		</div>
		</div>
	</div>
	<div id="main-grille">
		<a class="hvr-underline-reveal boxlink1" href="index.php/mon-demenagements/">
			<h3>DÉMÉNAGEMENT</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink2" href="index.php/service-lift/">
			<h3>SERVICE LIFT</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink3" href="index.php/garde-meubles/">
			<h3>GARDE-MEUBLES</h3>
			<h2>En savoir plus</h2>
		</a>
		<a class="hvr-underline-reveal boxlink4" href="index.php/fournitures/">
			<h3>FOURNITURES</h3>
			<h2>En savoir plus</h2>
		</a>
	</div>
</div>
<div class="page-wrap container fullwidth">
	<div id="main-content">
		<div class="main-content-wrap clearfix">
			<div id="content">

				<div id="left-area">

				<?php while ( have_posts() ) : the_post(); ?>

					<article class="entry-content clearfix">
					<?php if ( ! $featured_image ) : ?>
					<?php endif; ?>
					<?php
						the_content();

						wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Nexus' ), 'after' => '</div>' ) );
					?>
					</article> <!-- .entry -->

					<?php
						if ( comments_open() && 'on' == et_get_option( 'nexus_show_pagescomments', 'false' ) )
							comments_template( '', true );
					?>

				<?php endwhile; ?>

				</div> 	<!-- end #left-area -->

			</div> <!-- #content -->
		</div> <!-- .main-content-wrap -->

		<?php get_template_part( 'includes/footer-banner', 'page' ); ?>
	</div> <!-- #main-content -->

<?php get_footer(); ?>