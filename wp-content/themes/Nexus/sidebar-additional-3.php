<?php if ( is_active_sidebar( 'sidebar-additional-3' ) && 'on' === et_get_option( 'nexus_3rd_column', 'on' ) ) : ?>
	<div id="additional-sidebar-3">
		<?php dynamic_sidebar( 'sidebar-additional-3' ); ?>
	</div> <!-- #additional-sidebar -->
<?php endif; ?>